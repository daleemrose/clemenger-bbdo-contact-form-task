module.exports = function(grunt) {

	var PathConfig = require('./grunt-settings.js');
  
    grunt.initConfig({
    
        pkg: grunt.file.readJSON('package.json'),
        config: PathConfig,
        
        // JS Uglify
		uglify: {
			options: {
                preserveComments: true
            },
			dist: {
				files: {
				  '<%= config.jsDir %>/main.min.js': ['<%= config.jsSourceDir %>/**/*.js', '!<%= config.jsSourceDir %>/html5shiv.min.js']
				}
			}
		},
				
		// Imagemin
		imagemin: {
			dist: {
				options: {
					optimizationLevel: 7
				},
		        files: [{
		            expand: true,
		            cwd: '<%= config.imgSourceDir %>',  
		            src: ['**/*.{png,jpg,gif}'],
		            dest: '<%= config.imgDir %>' 
		        }]
		    }
		},
		
		// SVGmin
	    svgmin: {
			options: {
				plugins: [
					{
						removeViewBox: false
					}, {
						removeUselessStrokeAndFill: false
					}
				]
			},
			dist: {
				files: [
					{
						expand: true,
						src: ['**/*.svg'],
						cwd: '<%= config.imgSourceDir %>',
						dest: '<%= config.imgDir %>'
					}
				]
			}
	    },

	    // SASS / CSS
		sass: {
			options: {
				sourceMap: false,
				style: 'compressed'
			},
			dev: {
				files: {
					'<%= config.sassDir %>/<%= config.sassMainFileName %>.css': '<%= config.sassDir %>/<%= config.sassMainFileName %>.scss'
				}
			},
			prod: {
				files: {
					'<%= config.sassDir %>/<%= config.sassMainFileName %>.css': '<%= config.sassDir %>/<%= config.sassMainFileName %>.scss'
				}
			}
		},

		// CSS Min
		cssmin: {
			target: {
				files: {
					'<%= config.cssDir %>/<%= config.cssMainFileName %>.min.css': '<%= config.sassDir %>/<%= config.cssMainFileName %>.css'
				}
			}
		},
		
		//Copy PHP files
		copy: {
		  main: {
		  	files:
		  		[
			  		{
						expand: true,
						cwd: '<%= config.srcFolder %>/',
						src: '**/*.php',
						dest: '<%= config.distFolder %>/'
				    },
				    {
						src: '<%= config.jsSourceDir %>/html5shiv.min.js',
						dest: '<%= config.jsDir %>/html5shiv.min.js'
					}
				]
			}
		},
		
		//Process HTML
		processhtml: {
		    dist: {
		      files: {
		        '<%= config.distFolder %>/index.php': ['<%= config.srcFolder %>/index.php']
		      }
		    }
		},
		
		//minify html
		/*htmlmin: {
		
		    dist: {
		        options: {
		            removeComments: true,
		            collapseWhitespace: true
		        },
		
		        tasks: ['clean:php'],
		        files: {
		            '<%= config.distFolder %>/index.php': '<%= config.distFolder %>/index.php',
		        }
		    }
		},*/
		
		// Watch
		watch: {
		    options: {
		    	livereload: true
		    },
			markup: {
		        files: ['**/*.php', '**/*.html'],
		        options: {
		            livereload: true,
		        }
		    },	        
		    scripts: {
		        files: ['<%= config.jsSourceDir %>**/*.js'],
		        tasks: ['uglify'],
		        options: {
		        	livereload: true,
		            spawn: false
		        },
		    },
			sass: {
			    files: ['**/*.scss'],
			    tasks: ['sass:prod'],
			    //tasks: ['sass:prod', 'autoprefixer', 'cssmin'],
			    options: {
			    	livereload: true,
			        spawn: false
			    }
			}		    
		},

    });

	require('load-grunt-tasks')(grunt);
	
	/*grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-imagemin');
	grunt.loadNpmTasks('grunt-notify');
	grunt.loadNpmTasks('grunt-sass');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	//grunt.loadNpmTasks('grunt-contrib-htmlmin');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-processhtml');
	grunt.loadNpmTasks('grunt-svgmin');*/
	
	
	grunt.registerTask('default', ['watch', 'uglify', 'sass:dev', 'autoprefixer']);
	grunt.registerTask('dev', ['uglify', 'sass:dev']);
	grunt.registerTask('prod', ['uglify', 'sass:prod', 'autoprefixer', 'cssmin']);
	grunt.registerTask('dist', ['imagemin', 'svgmin', 'uglify', 'sass:prod', 'cssmin', 'copy', 'processhtml']);
	// processhtml is odd in that it is setup inside the html src itself, in this example it is:
		/*<!-- build:js js/main.min.js -->
		<script type="text/javascript" src="js/jquery.onepage-scroll.js"></script>
		<script type="text/javascript" src="js/modernizr.min.js"></script>
		<script type="text/javascript" src="js/main.js"></script>
		<!-- /build -->
		//this puts all of the above into simply <script src="js/main.min.js"></script>*/
	
};