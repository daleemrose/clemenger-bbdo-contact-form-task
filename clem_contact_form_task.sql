-- phpMyAdmin SQL Dump
-- version 4.2.5
-- http://www.phpmyadmin.net
--
-- Host: localhost:8889
-- Generation Time: Jan 10, 2016 at 10:59 PM
-- Server version: 5.5.38
-- PHP Version: 5.5.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `clem_contact_form_task`
--

-- --------------------------------------------------------

--
-- Table structure for table `clem_contacts`
--

CREATE TABLE `clem_contacts` (
`contact_id` int(11) NOT NULL,
  `contact_first_name` varchar(100) NOT NULL,
  `contact_last_name` varchar(100) NOT NULL,
  `contact_email` varchar(100) NOT NULL,
  `contact_message` varchar(140) DEFAULT NULL,
  `contact_date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `clem_contacts`
--
ALTER TABLE `clem_contacts`
 ADD PRIMARY KEY (`contact_id`), ADD KEY `contact_first_name` (`contact_first_name`), ADD KEY `contact_last_name` (`contact_last_name`), ADD KEY `contact_email` (`contact_email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `clem_contacts`
--
ALTER TABLE `clem_contacts`
MODIFY `contact_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;