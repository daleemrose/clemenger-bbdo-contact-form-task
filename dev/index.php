<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" class="no-js">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" >
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
	<title>Clemenger BBDO Contact Form</title>
	<?php //<meta name="description" content=""> ?>
	<?php //<meta name="keywords" content=""> ?>
	
	<!--[if lt IE 9]>
	<script src="js/html5shiv.min.js"></script>
	<![endif]-->
	
	<!-- build:css css/style.min.css -->
	<link rel="stylesheet" href="scss/style.css" />
	<!-- /build -->
</head>
<body>
	<?php $contactFirstName = $contactLastName = $contactEmail = $contactMessage = $honeypot = $company = $success = "";
	$firstNameError = $lastNameError = $emailError = $messageError = $generalError = ""; ?>
	<?php require('submit.php'); ?>
	<div id="contact-form">
		<h1>Clemenger <span class="redtext">BBDO</span></h1>
		<h2>Contact Form</h2>
		<p class="smalltext"><span class="redtext">*</span> Required field</p>
		<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
		
			<label for="ContactFirstName">First Name <span class="redtext">*</span></label>
			<?php if (!empty($firstNameError)) { ?>
				<span class="error"><?php echo $firstNameError; ?></span>
			<?php } ?>
			<input type="text" name="ContactFirstName" id="ContactFirstName" placeholder="First Name" value="<?php echo $contactFirstName; ?>" required />
			
			<label for="ContactLastName">Last Name <span class="redtext">*</span></label>
			<?php if (!empty($lastNameError)) { ?>
				<span class="error"><?php echo $lastNameError; ?></span>
			<?php } ?>
			<input type="text" name="ContactLastName" id="ContactLastName" placeholder="Last Name" value="<?php echo $contactLastName; ?>" required />
			
			<label for="ContactEmail">Email Address <span class="redtext">*</span></label>
			<?php if (!empty($emailError)) { ?>
				<span class="error"><?php echo $emailError; ?></span>
			<?php } ?>
			<input type="email" name="ContactEmail" id="ContactEmail" placeholder="Email Address" value="<?php echo $contactEmail; ?>" required />
			
			<label for="ContactMessage">Message <span class="char-limit redtext">(140 character limit)</span></label>
			<?php if (!empty($messageError)) { ?>
				<span class="error"><?php echo $messageError; ?></span>
			<?php } ?>
			<textarea placeholder="Message" id="ContactMessage" name="ContactMessage" maxlength="140" rows="6"><?php echo $contactMessage; ?></textarea>
			
			<div id="yearcheck">
				<label for="CurrentYear">Please leave this field blank <span class="redtext">*</span></label>
				<input type="text" name="CurrentYear" id="CurrentYear" placeholder="Current Year" value="<?php echo $honeypot; ?>" />
			</div>
			
			<div id="companycheck">
				<label for="Company">Do not change this field <span class="redtext">*</span></label>
				<input type="text" name="Company" id="Company" placeholder="Company" value="<?php if ($_SERVER["REQUEST_METHOD"] == "POST" && $company != "Clemenger BBDO") { echo $company; } else { echo "Clemenger BBDO"; } ?>" required />
			</div>
			
			<?php if (!empty($generalError)) { ?>
			<div id="generalerror">
				<span class="error"><?php echo $generalError; ?></span>
			</div>
			<?php } ?>
			
			<input type="submit" name="submit" value="submit" class="btn btn-block" /><?php if ($_SERVER["REQUEST_METHOD"] == "POST" && $success != "") { echo '<span class="success">'.$success.'</span>'; } ?>
		</form>
	</div>
	
	<!-- build:remove:dist -->
	<?php if (in_array($_SERVER['REMOTE_ADDR'], array('127.0.0.1', '::1'))) { // only echo this if we are locally deving :)
		echo '<script src="http://localhost:35729/livereload.js" type="text/javascript"></script>';
	} ?>
	<!-- /build -->
	<!-- build:js js/main.min.js -->
	<script type="text/javascript" src="js/jquery-1.11.3.min.js"></script>
	<script type="text/javascript" src="js/jquery.validate.min.js"></script>
	<script type="text/javascript" src="js/jquery.maxlength.min.js"></script>
	<script type="text/javascript" src="js/main.js"></script>
	<!-- /build -->
</body>
</html>