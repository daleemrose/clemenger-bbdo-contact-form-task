<?php

function secure_strip($input) {
	$input = trim($input);
	$input = stripslashes($input);
	$input = htmlspecialchars($input);
	return $input;
}

// Get form values posted
if ($_SERVER["REQUEST_METHOD"] == "POST") {
	/*if ($_SERVER["REQUEST_METHOD"] != "POST") {
		//echo 'you didnt submit a form did you?';
		header("Location: index.php");
		die();
	}*/
	
	$postedFirstName = secure_strip($_POST["ContactFirstName"]);
	$postedLastName = secure_strip($_POST["ContactLastName"]);
	$postedEmail = secure_strip($_POST["ContactEmail"]);
	$postedMessage = secure_strip($_POST["ContactMessage"]);
	$postedHoneypot = secure_strip($_POST["CurrentYear"]);
	$postedCompany = secure_strip($_POST["Company"]);
	
	//validate first name
	if (empty($postedFirstName)) {
		$firstNameError = "Required";
	} else {
		$contactFirstName = $postedFirstName; // this is so it returns correctly as a value
		// check if name only contains letters and whitespace
		if (!preg_match("/^[a-zA-Z ]*$/",$postedFirstName)) {
			$firstNameError = "Only letters and spaces allowed"; 
		}
	}
	
	
	//validate last name
	if (empty($postedLastName)) {
		$lastNameError = "Required";
	} else {
		$contactLastName = $postedLastName; // this is so it returns correctly as a value
		// check if name only contains letters and whitespace
		if (!preg_match("/^[a-zA-Z ]*$/",$postedLastName)) {
			$lastNameError = "Only letters and spaces allowed"; 
		}
	}
	
	
	if (empty($postedEmail)) {
		$emailError = "Required";
	} else {
		$contactEmail = $postedEmail; // this is so it returns correctly as a value
		// check if e-mail address is actually an email address...
		if (!filter_var($postedEmail, FILTER_VALIDATE_EMAIL)) {
			$emailError = "Invalid email address"; 
		}
	}
	
	
	if (strlen($postedMessage) > 140) {
		$messageError = "Trimmed to 140 characters";
	}
	$contactMessage = substr( $postedMessage, 0, 140); // this is so it returns correctly as a value
	
	
	if (!empty($postedHoneypot)) {
		$generalError = "Please try again";
	}	
	$honeypot = $postedHoneypot;
	
	
	if ($postedCompany != "Clemenger BBDO") {
		$generalError = "Please try again";
	}	
	$company = $postedCompany;
	
	
	//$contactFirstName = secure_strip($_POST["ContactFirstName"]);
	//$contactLastName = secure_strip($_POST["ContactLastName"]);
	//$contactEmail = secure_strip($_POST["ContactEmail"]);
	//$contactMessage = secure_strip($_POST["ContactMessage"]);
	
	// check for any errors
	if ($firstNameError == "" && $lastNameError == "" && $emailError == "" && $messageError == "" && $generalError == "") {	
		// Insert values into db
		require_once("dbinsert.php");
		require_once("sendemail.php");
		
		$success = "Successfully submitted your details &#10004; Thank you!";
		
		$contactFirstName = "";
		$contactLastName = "";
		$contactEmail = "";
		$contactMessage = "";
	}
}

?>