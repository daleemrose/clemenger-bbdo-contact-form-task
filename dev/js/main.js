$(document).ready(function($) {

	$('#yearcheck, #companycheck').hide();
	
	jQuery.validator.addMethod("accept", function(value, element, param) {
	  return value.match(new RegExp("." + param + "$"));
	});
	
	// validate signup form with jquery validate
	$("#contact-form form").validate({
		rules: {
			ContactFirstName: {
				required: true,
				minlength: 2,
				accept: "[a-zA-Z]+"
			},
			ContactLastName:{
				required: true,
				minlength: 2,
				accept: "[a-zA-Z]+"
			},
			ContactEmail: {
				required: true,
				email: true
			},
			ContactMessage: {
				maxlength: 140
			},
			/*CurrentYear: {
				maxlength: 0
			},*/
			Company: {
				maxlength: 14
			}
		},
		messages: {
			ContactFirstName: {
				required: "Required",
				minlength: "Must enter 2 or more characters",
				accept: "Must be letters not numbers"
			},
			ContactLastName: {
				required: "Required",
				minlength: "Must enter 2 or more characters",
				accept: "Must be letters not numbers"
			},
			ContactEmail: {
				required: "Required",
				email: "Invalid email address"
			},
			ContactMessage: "More than 140 characters"
		},
		errorPlacement: function(error, element) {
			$(element).before(error);
		}
	});
	
	//maxlength
	$('#ContactMessage').maxlength({
	    counterContainer: $("#char-limit")
	});
	
	//custom maxlength display
	$("#ContactMessage").bind("update.maxlength", function(event, element, lastLength, length, maxLength, left){
	    //console.log(event, element, lastLength, length, maxLength, left);
	    $(".char-limit").html("("+left+" characters left)");
	    if (left == 140) {
	    	$(".char-limit").html("(140 character limit)");
	    }
	});
	
});