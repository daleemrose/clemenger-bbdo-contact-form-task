module.exports = {
    sassDir:          'dev/scss',
    sassMainFileName: 'style',
    cssDir:           'dist/css',
    cssMainFileDir:   'dev/scss',
    cssMainFileName:  'style',
    jsDir:            'dist/js',
    jsSourceDir:      'dev/js',
    imgDir:           'dist/img',
    imgSourceDir:     'dev/img',
    srcFolder:		  'dev',
    distFolder:		  'dist'

    // sftp server
    /*sftpServer:       'example.com',
    sftpPort:         '2121',
    sftpLogin:        'login',
    sftpPas:          'password',
    sftpDestination:  '/pathTo/css'*/
  };